make: sshell.c
	gcc -o sshell sshell.c -Wall -Werror
	
.PHONY: clean

clean:
	rm -f *.o $(objects) sshell