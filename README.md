# ECS 150   : Building a Simple Shell

##### Author: Nausheen Sujela
##### SID: 914317423
##### Qtr: Winter 2018

The most critical design issues throughout this assignment were deciding which data structures we would use to store commands and how we would cleanly and efficiently parse through the user input to extract what we needed. I will be discussing both of these issues and how I chose to resolve them throughout this document.


#### Critical Note
I was not able to get my pipe function (runPipe) fully working. As such, I commented out the code that calls runPipe in main. I believe a few minor tweaks would fix the issues I am facing, but decided not to risk breaking the tester and so commented the calls out. The function runPipe can still be seen in the code.

## 1) Designing the 'command' and 'pipe' structs

I organized my program using two crucial data structures: the 'command' struct and the 'completePipe' struct. 

The 'command' struct contained details about individual commands. It stored the program name (e.g. "ls", "date", "echo", etc.) as well as the complete array of arguments associated with it. It also contained input file, output file, a boolean to record whether or not it involved input or output, and the total number of arguments.

The 'completePipe' struct contained the number of commands in the user input by parsing the input over the | symbol, as well as a char* array of the commands themselves. 

Further details of how I chose to design the creation of these structs are discussed below.

(Note: I completed Phases 0-4 without a command struct, choosing to parse and store all the arguments in a char* array. BIG mistake. I had to redesign a significant chunk of my code before I could even try to implement Phase 5 due to my poor initial design choice.)


## 2) Parsing user input

I chose to parse the command over four different symbols: |, <, >, and whitespace.

Because I decided to use strtok() to parse the strings, I made a copy of the original input string for each parse-through.

### Parsing over the pipe symbol

Every time the user enters input, it is first parsed over the | symbol. The purpose of this step is to separate the input into constituent commands in the case that the input was a piped command. 

This initial parse-through over the | symbol results in an instance of a completePipe struct, which contains the number of commands in the pipe as well as an array of char* containing the individual commands themselves. 

(If the user input is not truly a piped command, a completePipe structure will still be created but the number of commands will simply be 1 and the command array will contain just the one command.)

### Parsing over the redirection symbols

After the completePipe struct is created (suppose we call it "pipe"), we iterate over every command in pipe->all_commands[]. 

For each of _these_ commands, we parse over the redirection symbols, < and >, as well as whitespace, which I will discuss shortly.

For the input/output redirection symbols, we parse to determine and store the input/output file. Here, it was important to be mindful that there may or may not be whitespace between the words and the symbols. E.g. it could be grep lamb < mary.txt, or grep lamb <mary.txt, etc. This was particularly something to consider with input redirection.

I chose to separate out my code into four cases, each one dealing with a different case. It struck me later that it would be more clever to inject whitespace in cases where the words and symbols were not neatly separated and thus be able to treat all cases in the same way.

### Parsing over whitespace

The final step was to parse the command over whitespace. This step served to finish building the command struct by constructing the complete arguments array. Parsing over whitespace separated the command into its individual arguments. Each argument was stored into an array in the command struct.

After these four parse-throughs, the completePipe and command struct were fully built and ready to use. 


## References

I used Stack Overflow to help me out with some of the more mechanical (and for this assignment, trivial) functions such as removing a character from a string. Credit is given below.

* https://stackoverflow.com/questions/5457608/how-to-remove-the-character-at-a-given-index-from-a-string-in-c
